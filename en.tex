\documentclass[10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,left=3cm,right=3cm,top=3cm,bottom=3cm]{geometry}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{titling}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage[toc]{glossaries}  % needs to be after hyperref

% Adjust paragraph and text spacings
\setlength\parindent{0pt}
\setlength{\parskip}{8pt}
\renewcommand{\arraystretch}{1.5}
\renewcommand{\baselinestretch}{1.5}

% Describe title
\title{Evocracy}
\author{Carlo Michaelis, Patrick Charrier, Jannik Luboeinski}
\date{2023-09-11}

% Glossary entries
\newglossaryentry{author}{
  name=author, plural=authors,
  description={User who created a specific topic},
}
\newglossaryentry{coll-doc}{
  name=collaborative document, plural=collaborative documents,
  description={Document that is edited by all members of a group collaboratively},
}
\newglossaryentry{comrade}{
  name=comrade, plural=comrades,
  description={User who is part of a specific interest group},
}
\newglossaryentry{consensusstage}{
  name=consensus stage,
  description={Stage of a topic in which participants of the topic are assigned to groups to collaboratively work on a solution; it consists of several consecutive levels; takes place after proposal stage},
}
\newglossaryentry{degreeconsensus}{
  name=degree of consensus,
  description={Value representing how many participants of a topic approve the final document of the decision process},
}
\newglossaryentry{delegate}{
  name=delegate, plural=delegates,
  description={Elected representative of a group; enters the next level of the consensus stage},
}
\newglossaryentry{follower}{
  name=follower, plural=followers,
  description={User who is interested in the activity of another user, which may entail following that user's posts},
}
\newglossaryentry{group}{
  name=group, plural=groups,
  description={Participants are assigned to different groups for each level of the consensus stage (the last level features a single group)},
}
\newglossaryentry{interestgroup}{
  name=interest group, plural=interest groups,
  description={Group for a specific purpose that is organized in a centralized manner},
}
\newglossaryentry{level}{
  name=level, plural=levels,
  description={Substage of the consensus stage; the number of levels is determined by the number of participants and the targeted size of the groups},
}
\newglossaryentry{location}{
  name=location, plural=locations,
  description={One of several possible residences of a user; is verified if a sufficient number of other users has confirmed it; a verified location is necessary to enter a reference group and related topics},
}
\newglossaryentry{groupmember}{
  name=group member, plural=group members,
  description={Person who has been assigned to a specific group in the consensus stage}
}
\newglossaryentry{observer}{
  name=observer, plural=observers,
  description={User who has not submitted a proposal or who has submitted an invalid proposal for a topic (if the proposal is valid, the user is a participant)},
}
\newglossaryentry{participant}{
  name=participant, plural=participants,
  description={User who has submitted a valid proposal for a topic (any user is an observer otherwise)},
}
\newglossaryentry{post}{
  name=post, plural=posts,
  description={Piece of content that a user sends out to their followers, a reference group, and/or an interest group},
}
\newglossaryentry{pot-participant}{
  name=potential participant, plural=potential participants,
  description={User who has the permission to submit a proposal for a specific topic (also see `target group')},
}
\newglossaryentry{proposal}{
  name=proposal, plural=proposals,
  description={An individual solution approach written by a user for a specific topic},
}
\newglossaryentry{proposalstage}{
  name=proposal stage,
  description={Stage of a topic in which proposals can be submitted; takes place after selection stage and before consensus stage},
}
\newglossaryentry{referencegroup}{
  name=reference group, plural=reference groups,
  description={Group for a specific purpose that is organized in a decentralized manner (e.g., considering geographic regions)},
}
\newglossaryentry{relevance}{
  name=relevance, plural=relevances,
  description={Value that users assign to a topic to cause its launch, depending on a threshold of relevance that has to be reached},
}
\newglossaryentry{runtimeparameter}{
  name=runtime parameter, plural=runtime parameters,
  description={Parameter that changes the behavior of the software; is chosen democratically and in a decentralized manner by users},
}
\newglossaryentry{selectionstage}{
  name=selection stage,
  description={Stage of a topic in which users can mark the topic as relevant; leads to the decision if a topic shall be discussed or not; takes place before proposal stage},
}
\newglossaryentry{targetgroup}{
  name=target group, plural=target groups,
  description={Group of users that has the permission to write a proposal for a specific topic (also see `potential participant')},
}
\newglossaryentry{topic}{
  name=topic, plural=topics,
  description={Statement of a problem that is to be discussed and for which a solution is sought through a democratic decision process}
}
\newglossaryentry{user}{
  name=user, plural=users,
  description={Person who is registered in a running instance of OpenEvocracy},
}

% Format glossary entries to italic
\renewcommand{\glstextformat}[1]{\textit{#1}}

% Generate glossary
\makeglossaries

\begin{document}

\begin{center}

\vspace*{1.5cm}
{\Huge \thetitle}
\vspace*{0.2cm}

{\Large An Evolutionary Web3 Democracy}
\vspace*{1.0cm}

{\large \theauthor}
\vspace*{0.2cm}

develop@openevocracy.org
\vspace*{-0.3cm}

\href{https://openevocracy.org/}{openevocracy.org}
\vspace*{1.7cm}

{\large \thedate}

Whitepaper v1.2

\end{center}

\vspace*{\fill}

\begin{center}
This work is published under\\ \href{https://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution 4.0 International (CC BY 4.0)}.
\end{center}

\thispagestyle{empty}
\newpage

%%%%%%%%%%%%%%%%%%%%
%%%%% ABSTRACT %%%%%
%%%%%%%%%%%%%%%%%%%%

\section*{Abstract}

Evocracy is a concept for organizing democratic decision-making, which uses modern information technology for this purpose. The goal is to enable high quality decision-making, decentralize decision-making processes, and at the same time ensure as much anonymity and security as possible. At the center of the concept are user-created topics. These define a question or problem statement on which a decision is to be made.

Discussions on a topic are outsourced to small groups. All members of a group work on a collaborative document. Because of the small groups, every idea has a chance to be considered. Based on their topic-specific knowledge and ability to unify ideas, delegates from the groups are elected to higher levels where they again form small groups with other elected delegates. The number of participants and groups is thus reduced from level to level until a single document remains. Through the process, greater consideration is given to those ideas that prove to be reasonable and consensual across multiple groups in discussions. Through this self-organized process, good ideas are selected in an evolutionary sense.

Evocracy is free of explicit authority; all users have equal rights. Every user has the right to propose topics and can participate in any topic. To prevent abuse, a user's location and authenticity are verified in a decentralized manner, i.e., through mutual confirmation and evaluation. However, there is no way to identify users. Each topic is assigned to one of many possible target groups, which can dynamically emerge from the relationships of users' locations, regardless of existing structures (such as states, municipalities, etc.).
\newpage

\tableofcontents
\newpage

%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% INTRODUCTION %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

The Evocracy concept deals with decision-making and therefore aims to enable an efficient legislation. There are two basic problems to be solved:

\begin{enumerate}
    \item How can we find out if a \gls{topic} is \glsdisp{relevance}{relevant}?
    \item How can we find a good solution for a \gls{topic}?
\end{enumerate}

The approach of Evocracy regarding the first problem is to allow \glspl{user} to mark certain \glspl{topic} as \glsdisp{relevance}{relevant}. Further, there are mechanisms enabling that those \glspl{participant} who are capable of contributing good content and/or have a high sensitivity regarding the \gls{topic} gain influence during the decision-making process for a given \gls{topic}. The name Evocracy is derived from the evolutionary character of this approach and the grassroots-like contribution of all \glspl{participant}. We call the associated open source software OpenEvocracy.

%\textcolor{red}{Abgrenzung zu anderen digitalen Demokratieprojekten?}

%%%%%%%%%%%%%%%%%%%
%%%%% PROCESS %%%%%
%%%%%%%%%%%%%%%%%%%

\section{Decision-making process}

In order to make a decision jointly with many persons, a \gls{topic} will go through different phases, which are explained below.

\subsection{Creating and selecting topics (selection stage)}

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.5\textwidth]{schwellen_v3.pdf}
	\caption{The thresholds for the number of required relevance flags determine when a topic is accepted for discussion. Many different courses are possible, three examples are depicted in this figure: (a) The number of relevance flags increases steadily and eventually exceeds the upper threshold, which leads to a start of the topic discussion. (b) The number of relevance flags initially increases, but then decreases. After being below the lower threshold for time $\Delta t \geq \Delta t_\text{reject}$, the topic is discarded. (c) The number of relevance flags initially increases, then temporarily decreases again, is below the lower threshold for a time $\Delta t < \Delta t_\text{reject}$, but then increases again until it exceeds the upper threshold. The topic is accepted and the discussion can start.} \label{fig.schwellenwerte}
\end{figure}

In principle, each \gls{user} has the possibility to create a \gls{topic}. A \gls{topic} is defined by a headline, a description text, and a \gls{targetgroup} (either a \gls{referencegroup} or \gls{interestgroup} such as ``Berlin'', ``Germany'', ``Rockfestival 2020'' or ``Human Resources Department'', or a specific geographic area; see Fig. \ref{fig.zielgruppen_empfaengergruppen}). The type, \glspl{referencegroup} and/or \glspl{interestgroup}, depends on the particular use-case of the OpenEvocracy instance. All \glspl{user} belonging to the \gls{targetgroup} can participate in the \gls{topic}, thus, they are \glspl{pot-participant}.

All \glspl{user} can contribute their opinions and comments to any \gls{topic}. To promote high standards, \glspl{user} can add literature, e.g., scientific studies, to the description of a \gls{topic}. This will allow that other interested \glspl{user} can get a differentiated and clear impression of a specific \gls{topic}.

In order to select \glspl{topic} for discussion, all \glspl{user} have the possibility to judge their \gls{relevance}. For this purpose, two thresholds are defined. These depend on the reference number $N_{\text{ref}} \le N_{\text{tot}}$, where $N_{\text{tot}}$ is the number of all \glspl{user} and $N_{\text{ref}}$ is the number of the \glsdisp{pot-participant}{potential participants}. The number of \glspl{user} that have marked a \gls{topic} as \glsdisp{relevance}{relevant} is denoted by $K$. A \gls{relevance} vote expires if the \gls{user} has not opened the \gls{topic} for a certain period of time (e.g., 6 months), and does not mark the \gls{topic} as \glsdisp{relevance}{relevant} again. If the ratio $K/N_{\text{ref}}$ of a \gls{topic} reaches a certain upper threshold, the \gls{topic} is considered \glsdisp{relevance}{relevant} and the discussion process starts automatically. If $K/N_{\text{ref}}$ falls below a lower threshold, the \gls{topic} is considered \glsdisp{relevance}{irrelevant} and is subsequently removed. This process is called the \gls{selectionstage}, examples are shown in Fig. \ref{fig.schwellenwerte}.

The thresholds are defined as follows:

\begin{itemize}
    \item \textbf{Upper threshold}: If $K/N_\text{ref} \geq S_+$, where $S_+$ is the upper threshold, the \gls{topic} is considered as \glsdisp{relevance}{relevant} and the discussion starts.
    \item \textbf{Lower threshold}: If $K/N_\text{ref}$ stays below the lower threshold $S_-$ for a time period of $\Delta t \geq \Delta t_\text{reject}$, the \gls{topic} is rejected. If $S_-$ is crossed ``from below'' within $\Delta t < \Delta t_\text{reject}$, it leads to a reset of $\Delta t$, i.e. $\Delta t=0$.
\end{itemize}

\subsection{Proposals (proposal stage)}

When a \gls{topic} has been accepted for discussion, each \gls{user} has the opportunity to write a \gls{proposal} on the \gls{topic}. In addition to detailed solution proposals, simple opinions, wishes or fears can also be conveyed. The \gls{proposal} can only be edited within a time period $T_P$.

A minimum number of words $N_P$ is required for the \gls{proposal}, which intends to motivate the \glspl{participant} to independently think about the \gls{topic} and to engage with the collected literature from the \gls{selectionstage}. On the other hand, the minimum number of words should be kept low to allow as many \glspl{user} to participate as possible.

The \gls{proposalstage} ends when the aforementioned period $T_P$ has expired. Further editing of the \glspl{proposal} is no longer possible after that period. \Glspl{proposal} with more than $N_P$ words are accepted as valid. All \glspl{user} with valid proposals will become \glspl{participant} of the decision process on the given \gls{topic}. They will be notified that they can participate in the following \gls{consensusstage}. All other \glspl{user}, who have not reached the minimum number of words $N_P$ or have not created a \gls{proposal}, become \glspl{observer} of the decision process. \Glspl{observer} can only indirectly influence the following \gls{consensusstage}, for example, through external forums (see below).

\subsection{Decision making (consensus stage)}

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.8\textwidth]{evolution-tree_en_v3.pdf}
	\caption{The evolutionary principle of the Evocracy concept. The colored squares depict members of group 1, colored circles members of group 2 and the colored lines in the documents depict their proposals. Each group develops a common proposal, which shall be conveyed to the next level by an elected delegate. Thereby, good ideas from earlier groups are transferred to groups of later levels. Likewise, users with high skills are more likely to enter later levels.}
	\label{fig.evolution_tree}
\end{figure}

In the first \gls{level} of the \gls{consensusstage}, all \glspl{participant} are randomly divided into \glspl{group} of size $n_G$ (e.g., $n_G=5$). Since joint discussions in large \glspl{group} of possibly several hundreds of \glspl{groupmember} are difficult, it is sensible to form rather small \glspl{group}. 

Within a \glspl{group}, \glspl{groupmember} are randomly assigned names and colors. These apply exclusively to a specific \gls{group} and are not gender-specific. In this way, the \glspl{groupmember} have the option to remain anonymous at any time. Nevertheless, they can view the \glspl{proposal} of all other \glspl{groupmember}. In addition, an empty \gls{coll-doc} is provided\footnote{It is a future goal that the \gls{coll-doc} is initially filled with a proposal template automatically generated by a machine learning algorithm, based on the previous \glspl{proposal} of all \glspl{groupmember}. This algorithm should be optimized as the number of \glspl{topic} increases.}, and all \glspl{groupmember} have the permission to write in it. Through communication tools (e.g., chat, forum, scheduling, polls, voting), the \glspl{groupmember} can discuss their positions before recording the results in the \gls{coll-doc}. Ideally, the \gls{group} reaches a consensual solution for the problem. If this is not the case, the \gls{group} is free to record disagreements and uncertainties in the document.

Writing the joint \gls{proposal} in the \gls{coll-doc} is accompanied by an evaluation process. The \glspl{groupmember} evaluate each other and themselves according to three criteria:

\begin{itemize}
    \item \textbf{Cooperativeness}: How well does the \gls{groupmember} cooperate? Is the person able to make compromises? Is the \gls{groupmember} interested in comprehending all positions and not categorically excluding any? Does the person have the ability to find a new perspective for different positions that will gain higher acceptance?
    \item \textbf{Knowledge in the area of the topic}: Has the \gls{groupmember} the ability to argue based on facts? Is the person well prepared for the \gls{topic}? Can the person deal with the \gls{topic} in a differentiated manner?
    \item \textbf{Invested time}: Is the \gls{groupmember} regularly available? Does the \gls{groupmember} continuously participate in the discussion and writing process? Does the \gls{groupmember} respond promptly to discussions in the chat and/or in the forum?
\end{itemize}

Just as the \glspl{proposal} in the previous stage, the \gls{coll-doc} of a \gls{group} in a certain \gls{level} of the \gls{consensusstage} can again only be edited for a limited amount of time. After this time expires, the evaluation process is automatically finalized, resulting in a \gls{delegate} being designated to represent the \gls{group}. All other \glspl{groupmember} become \glspl{observer} and can no longer influence the process directly.

The elected \glspl{delegate} are again randomly assigned to \glspl{group} of size $n_G$, provided with a new \gls{coll-doc}, and are randomly assigned new names and colors. The \gls{consensusstage} thus reaches a new \gls{level}. The process continues until a single \gls{group} remains at last which elaborates the final document for the \gls{topic}.

\subsubsection{Forums}

\Glspl{observer} have no write access to the \glspl{coll-doc} within the \glspl{group}. However, they can view the documents at any time. In addition to the \gls{coll-doc}, each \gls{group} has a forum where all \glspl{user} can participate. Within a forum, specific parts of text in the \gls{coll-doc} of the \gls{group} can be referenced.

Through the forum, the \glsdisp{groupmember}{members} of the respective \gls{group} have the opportunity to be inspired by and respond to external suggestions from \glspl{observer} at any time, while their work within the \gls{group} is kept separate.

Some advantages of using forums to involve all \glspl{user} to the discussion process of a \gls{topic} are:

\begin{itemize}
    \item All \glspl{user}, including those who did not submit a \gls{proposal} at the beginning, can contribute ideas, wishes or criticism. Ideas that were lost in the course of the process can be put forward again.
    \item A transfer of ideas between different \glspl{group} is enabled. Decisions are thus optimized not only vertically between \glspl{level}, but also horizontally between the \glspl{group} within one \gls{level}.
    \item \Glspl{participant} from previous \glspl{level} can inform the \glsdisp{groupmember}{members} of the current \gls{group} about changed behavior of their \gls{delegate} and thus indirectly influence the further evaluation of the \gls{delegate}.
\end{itemize}

\subsubsection{Degree of consensus}

At the end of the last \gls{level} of the \gls{consensusstage}, a vote on the final document takes place. Thereby, the extent to which all of the original \glspl{participant} of a \gls{topic} agree with the final result is examined. The percentage of agreement is called the \gls{degreeconsensus}.

Regardless of the \gls{degreeconsensus}, the final document that has been developed through the stages remains in place. If a majority of the \glspl{participant} are not satisfied with the final result, however, \glspl{participant} may decide to re-create the \gls{topic} such that it has a chance to be discussed and developed again.
%\textcolor{red}{wir könnten das noch präziser ausformulieren}

\subsection{Cleanup of topics}

It is intended that some contents of a completed topic are endowed with an expiration date and are thus deleted after a certain time. First, after a time $t_\text{df}$ after the termination of the \gls{topic}, the forums within the \glspl{group} are deleted. Next, after a time $t_\text{dg} > t_\text{df}$, the entire \glspl{group} will be deleted. The final document remains, as does important metadata, such as statistics about the number of \glspl{group}, group sizes, levels, contributions, as well as the number of votes for and against the final document.

The purpose of partly deleting the topic-related data is to reduce the load on the infrastructure and, for data protection reasons, to store only as much data as is absolutely necessary.
%\textcolor{red}{Wollen wir das wirklich in jedem Fall?}

%%%%%%%%%%%%%%%%%%%%%
%%%%% STANDORTE %%%%%
%%%%%%%%%%%%%%%%%%%%%

\section{Locations and reference groups}\label{sec.locations_reference_groups}

% Check proof of location (FOAM)

The \glspl{location} and the membership for \glspl{referencegroup} are controlled by a so-called web of trust. This entails that \glspl{user} confirm several \glspl{location} (residences or whereabouts) for each other.

\subsection{Verification of locations}

Each user can choose several \glspl{location} (residences or whereabouts). In order to get a new \gls{location} verified, the software first determines the coordinates of the current position (e.g., via GPS or Galileo) and suggests the \gls{user}'s nearest \gls{location}. If the \gls{user} has not yet selected a \gls{location}, they can either use the coordinates of the current position directly as the new \gls{location}, or modify them as desired. The selected \gls{location} is confirmed by other \glspl{user}, hereafter referred to as reviewers. The farther away the \gls{user}'s location is from a reviewer's closest verified \gls{location}, the lower is the weight of the reviewer's confirmation value. The sum of the weighted confirmation values must exceed a certain threshold for a \gls{user}'s \gls{location} to be verified.

A confirmation has the value $1$ if the \gls{location} of the \glspl{user} and the reviewer is identical, and it has the value $0$ if the \glspl{location} of both are on the exact opposite sides of the earth. The weighting function between these two points is nonlinear. The threshold for the verification of a \gls{location} should be set relatively high to reduce the action of bots and to motivate \glspl{user} to choose their \glspl{location} deliberately.

Every confirmation of a \gls{location} by a reviewer is stored in a list, which is visible to all \glspl{user}. A confirmation expires after a certain period of time (e.g., 2 years), but can be reactivated without personal contact by mutual confirmation. Analogously to a friend request in social networks, the reactivation must be initiated manually by one of the two \glspl{user} involved. A reactivation should be possible well before the confirmation expires (e.g., after 1 year), and once it becomes possible, the software will indicate this. The reactivation process should be very easy. Optionally, for example, for time-limited events, the expiration time for a confirmation can be chosen lower than the default value.

If a \gls{user}'s \gls{location} has not had confirmations for a certain period of time (e.g., for 3 months), then the \gls{location} will be deleted automatically. A \gls{user} can only have a certain number (e.g., 1 or 2) of new, unverified \glspl{location} in addition to their already verified \gls{location}. If the \gls{user} wishes to choose additional \glspl{location}, the previously selected \glspl{location} must first be verified or the \gls{user} must wait until a \gls{location} is expired. This aims to prevent misuse of the \gls{location} functionality, especially concerning bots.

\subsection{Verification of reference groups}

Each \gls{user} can be a member of different \glspl{referencegroup} (indicated by hashtags). To become part of a \gls{referencegroup}, the \gls{user} selects a verified \gls{location} (if no \gls{location} has been verified yet, joining a \glspl{referencegroup} is not possible). The program will then suggest potentially relevant \glspl{referencegroup} for the \gls{user}, i.e., \glspl{referencegroup} that are in geographic proximity. The \gls{user} can then pick  \glspl{referencegroup} and get them confirmed by other \glspl{user} (see below for more details of this procedure). It is also possible to create a new \gls{referencegroup}. The \glspl{location} of all \glspl{user} that have received confirmation for a given \gls{referencegroup} define the geographic extent of the \gls{referencegroup}. This manifests itself in a density distribution to which all confirmed \glspl{location} of the \gls{referencegroup} contribute (as described below, confirmations are weighted by the confirming \gls{user}'s distance to the associated \gls{location}).
%\textcolor{red}{$\leftarrow$ Das hier habe ich in Klammern gesetzt, eventuell kann man es auch wegstreichen oder unten einfügen. Generell könnten wir diesen ganzen Abschnitt noch deutlich kompakter machen.}

The confirmation of a \gls{referencegroup} by one \gls{user} for another \gls{user} has to happen with respect to a specific \gls{location}. This \gls{location} must be selected by the \gls{user} who wants to receive the confirmation.
%\textcolor{red}{$\leftarrow$ Soll der bestätigende User auch seine Location wählen können? Oder wird die nächstgelegene genommen? $\to$ Die nächstliegende wird standardmäßig genommen. Da aber sowieso der Standort manipuliert werden kann, kann man den Usern auch gleich die Möglichkeit geben einen Standort zu wählen (bzw. die Standardauswahl durch GPS zu korrigieren). Die Reviewer entscheiden ja dann, ob der Standort ok ist oder nicht.}
All confirmations received for a certain \gls{referencegroup} are weighted with the density distribution of the \gls{referencegroup} at the selected \gls{location}, and then summed up. The confirmations for a \gls{referencegroup} may be distributed over the different \glspl{location} held by a \gls{user}. A \gls{referencegroup} is considered verified for this \gls{user} if the sum of the weighted confirmations has exceeded a certain threshold $S_\text{g}$.

Confirmations of \glspl{referencegroup} have an expiration date, analogously to the confirmations of a \gls{location}. To avoid the effort for collecting confirmations, especially, for multiple \glspl{referencegroup}, the threshold $S_\text{g}$ should be set rather low.

\subsection{Additional remarks}

The number of confirmations for \glspl{location} and \glspl{referencegroup} that a \glspl{user} can provide for other users within a certain period of time (e.g., per week) is limited. Unused confirmations expire when this time has elapsed. In the next period, the same fixed number of confirmations is available, i.e. the number of possible confirmations is not cumulative.

The spatial extents of \glspl{referencegroup} may overlap. For example, the extent of the \gls{referencegroup} ``\#hamburg'' would presumably be a subset of the extent of the \gls{referencegroup} ``\#germany'', and the extent of the \gls{referencegroup} ``\#formele'' of an event in Berlin is likely a subset of the extent of the \gls{referencegroup} ``\#berlin''.

A \gls{topic} may be related to multiple \glspl{referencegroup}. If there are two competitive \glspl{referencegroup}, for example, ``\#gottingen'' and ``\#goettingen'', both can be added to the \gls{topic}. In this case, all groups of \glspl{user} are merged and selected in an unambiguous manner.

%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% DEZENTRALITAET %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Decentralization}

\subsection{Technical decentralization}

In our vision, OpenEvocracy will be fully realized as a Web3 application, with the related data stored in a traceable and transparent way. In the long run, OpenEvocracy shall be designed in a way that no central nodes are used, in order to prevent that processes can possibly be manipulated by server operators. Thereby, powerful institutions such as countries or tech companies can not directly interfere with the network architecture and thus with the decision-making processes of OpenEvocracy, which will serve to ensure trust in the system.

Distributed Ledger Technology (DLT), e.g., in the form of a blockchain will be used to implement the decentralized data storage. Potential DLTs that may be used are Ethereum (\href{https://ethereum.org}{ethereum.org}), Polkadot (\href{https://www.polkadot.network/}{polkadot.network}), EOS (\href{https://www.eos.io}{eos.io}), or DFINITY (\href{https://dfinity.org}{dfinity.org}). These technologies ensure that the data stored is valid. That means, data is stored only following the algorithm, which does not allow manipulation as by a central server operator. However, not all DLTs enable the storage of larger amounts of data. For decentralized storage of data and databases, the InterPlanetary File System (IPFS; \href{https://ipfs.io}{ipfs.io}) with OrbitDB (\href{https://github.com/orbitdb}{github.com/orbitdb}) can be used. Further, only a few DLT projects currently support the decentralized provisioning of frontends. A fully decentralized software will therefore consist of a combination of different technologies and possibly even depends on new developments in the field of decentralized technology.

Note that as explained in section~\ref{sec.locations_reference_groups}, the verification of \glspl{location} and \glspl{referencegroup} is performed via a web of trust, where trust is shared among \glspl{user} in a decentralized manner. 
%\glspl{user} express trust in each other

\subsection{Content-related decentralization}

Upon the installation of OpenEvocracy, some parameters of the system can be set initially. Many other parameters, so-called \glspl{runtimeparameter} (thresholds, processing time for the phases of a \gls{topic}, group size, etc.), are chosen democratically and decentrally at system runtime by all \glspl{user}. A location parameter (e.g., the mean) of the chosen value from all \glspl{user} is used as a dynamic \gls{runtimeparameter}. Unlike in centrally organized networks, there are no user roles in OpenEvocracy. At the time of their registration in the system, all \glspl{user} have the same permissions. However, for which \glspl{runtimeparameter} a \gls{user} can propose values depends on the user's karma (see below).

The \gls{author} of a \gls{topic} assigns a certain region, i.e., a geographic coordinate with a certain radius or a \gls{referencegroup}, to the \gls{topic}. \Glspl{user} who are verified within this reference range (see section~\ref{sec.locations_reference_groups}) have the possibility to participate in the \gls{topic}. They are the $N_{\text{ref}}$ \glspl{pot-participant}. This explicitly enables to discuss \glspl{topic} independent of existing social structures (e.g., independently of country borders).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% SOZIALES NETZWERK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{User account and social network}

\subsection{Anonymous user account}

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.8\textwidth]{zielgruppen_empfaengergruppen_en_v2.pdf}
	\caption{Possible types of target groups (in relation to topics) and recipient groups (in relation to posts in the social network).}
	\label{fig.zielgruppen_empfaengergruppen}
\end{figure}

Any real person can create a user account, which consists of an e-mail address and a password. It is not possible to add pictures, a username, or other personal data beyond the mentioned.

When accounts are thus anonymous, bots and trolls can become a huge issue. Also, decision processes might be deliberately manipulated by one and the same person with multiple \gls{user} accounts. Therefore, \glspl{user} have a karma value $K$ which helps other \glspl{user} to assess the authenticity of a \gls{user} account. The karma value is composed of various sub-values. These sub-values include reputation (e.g., the balance of upvotes and downvotes on \gls{user} posts), trust (e.g., size of user's social network and degree of isolation), and position confirmations (e.g., number of confirmed \glspl{location}, number of confirmed \glspl{referencegroup}). Depending on the global karma value or on sub-values, \glspl{user} are granted or revoked certain permissions (e.g., to create \glspl{topic}, or to downvote comments). If the global karma value falls below a certain threshold $K<K_\text{ban}$, the \gls{user} is assumed to be fake and blocking measures are initiated.

In order to prevent the creation of fake accounts even before they can be registered, \glspl{user} are only admitted to the network via invitations, where each \gls{user} is only allowed to cast a certain number of invitations (e.g., $1$) per period of time (e.g., per week).

\subsection{Social network}

\Glspl{user} can connect with each other in order to find interesting \glspl{topic}, gather content-related inspiration, and share news. A piece of information sent by a \gls{user} to their social network will be called a \gls{post}. A \gls{post} can be restricted to a \gls{referencegroup} (see above), an \gls{interestgroup} and/or to so-called \glspl{follower} (see Fig. \ref{fig.zielgruppen_empfaengergruppen}). \Glspl{follower} and \glspl{interestgroup} are described below.

\Glspl{user} who are interested in the \glspl{post} of other \glspl{user} may follow them; they are then called a \gls{follower}. The \gls{follower} relationship is unidirectional, i.e., it does not require mutual acknowledgment, and it is anonymous. \Glspl{user} thus have the opportunity to learn about the activity of other \glspl{user} through a timeline of \glspl{post}. This will provide them with potentially \glsdisp{relevance}{relevant} \glspl{topic} and general information.

\Glspl{user} who share some common interest may join in so-called \glspl{interestgroup}, whose members are called \glspl{comrade}. An \gls{interestgroup} can be founded by any \gls{user} and does not require a unique name. The founding \gls{user} is automatically the administrator of the \gls{interestgroup} who can grant other \glspl{user} access. The administrator can also rename or delete the \gls{interestgroup}, or grant other \glspl{user} administrator permissions. An \gls{interestgroup} can be assigned to a \gls{topic} as its \gls{targetgroup}, which will allow only the \glspl{comrade} of an \gls{interestgroup} to join the discussion as \glspl{pot-participant} of the \gls{topic}. Such \glspl{topic} are referred to as \emph{closed} \glspl{topic}. In addition, for further exchange and discussion a forum will be available. Through this, \glspl{comrade} of an \gls{interestgroup} have the opportunity to notify each other about potentially \glsdisp{relevance}{relevant} \glspl{topic}, jointly prepare new \glspl{topic}, and coordinate unified action in public \glspl{topic}.

\newpage

%%%%%%%%%%%%%%%%%%%%
%%%%% APPENDIX %%%%%
%%%%%%%%%%%%%%%%%%%%

\section{Appendix}

\begin{table}[!h]
    \footnotesize
    \centering
    \begin{tabular}{p{.45\textwidth}p{.02\textwidth}p{.45\textwidth}}
      \textbf{Reference group}   && \textbf{Interest group} \\  [+0.5em] \hline
      
      Organized decentrally & & Organized centrally \\ \hline

      Can be a recipient for \glspl{post} in the social network & & Can be a recipient for \glspl{post} in the social network \\ \hline
      
      Can be a \gls{targetgroup} for \glspl{topic} && Can be a \gls{targetgroup} for \glspl{topic} \\ \hline

      \Glspl{topic} can be assigned to it, these \glspl{topic} are labeled \emph{open} && \Glspl{topic} can be assigned to it, these \glspl{topic} are labeled \emph{closed} \\ \hline
      
      \emph{Open}, i.e., in principle accessible for all \glspl{user} && \emph{Closed}, i.e., only accessible for \glspl{user} with permission \\ \hline
      
      \emph{Unmanaged}; there is no administrator, a \gls{referencegroup} can ``emerge and fade'' && \emph{Managed}; administrators decide about adding new \glspl{comrade}, granting administrator permission and deleting/renaming the \gls{interestgroup} \\ \hline

      \emph{Decentralized verification} by other \glspl{user} via algorithm && \emph{Centralized verification} by administrator(s) \\ \hline

      \emph{Name is unique}, ``\#berlin'' can exist only once && \emph{Name is not unique}, ``Berlin'' can exist multiple times, only ID is unique \\ \hline
      
    \end{tabular}
    \caption{Difference between \glspl{referencegroup} and \glspl{interestgroup}.}
    \label{tab:difference-bg-ig}
\end{table}

\newpage

%%%%%%%%%%%%%%%%%%%
%%%%% GLOSSAR %%%%%
%%%%%%%%%%%%%%%%%%%

\printglossaries

\end{document}
