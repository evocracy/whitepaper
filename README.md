# Evocracy Whitepaper

Evocracy it the concept behind the OpenEvocracy Software (https://openevocracy.org/).

You can directly download the latest whitepaper here:

https://openevocracy.org/files/Evocracy-Whitepaper_v1-2_2023-09-11.pdf

## License

[Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)

